import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from  '@angular/common/http';
import { DataService } from './data.service';
import { UserListComponent } from './Users/user-list/user-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserListItemsComponent } from './Users/user-list/user-list-items/user-list-items.component';
import { UserListModalComponent } from './Users/user-list-modal/user-list-modal.component';
import { UserComponent } from './Users/users.component';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserListItemsComponent,
    UserListModalComponent,
    UserComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ModalModule.forRoot()
    ],
    entryComponents: [
      UserListModalComponent
    ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
