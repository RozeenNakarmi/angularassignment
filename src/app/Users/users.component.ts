import { Component, OnInit, TemplateRef, Output, Input, ViewChild, ElementRef } from '@angular/core';
import { IUser } from '../model/user.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UserListModalComponent } from './user-list-modal/user-list-modal.component';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UserComponent implements OnInit {
  selectedUser: IUser;
  modalRef: BsModalRef;
  
  constructor(private _modalServce: BsModalService) { }

  ngOnInit() {
  }
  openModal(selectedUser: IUser){
    this.modalRef = this._modalServce.show(UserListModalComponent, {initialState: {user:selectedUser}});
  }
}
