import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from 'src/app/model/user.model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserListModalComponent } from '../../user-list-modal/user-list-modal.component';


@Component({
  selector: 'app-user-list-items',
  templateUrl: './user-list-items.component.html',
  styleUrls: ['./user-list-items.component.css']
})
export class UserListItemsComponent implements OnInit {
  @Input() user: IUser;
  @Output() userSelected = new EventEmitter<void>();

  constructor() {
    
   }

  ngOnInit() {
   
  }
  onSelected()
  {
     this.userSelected.emit();
  }
}
