import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../data.service';
import { IUser } from '../../model/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public users = [];
  @Output() userWasSelected = new EventEmitter<IUser>();

  constructor(private _dataService:  DataService)
  {

  }
  ngOnInit(){
    this._dataService.getUsers()
    .subscribe(data => this.users =data);
  }
  onUserSelected(user: IUser)
  {
    this.userWasSelected.emit(user);
  }

}

