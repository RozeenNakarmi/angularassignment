import { Component, OnInit, Input, ContentChild, 
  ElementRef, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IUser } from 'src/app/model/user.model';
import { BsModalRef } from 'ngx-bootstrap/modal/';

@Component({
  selector: 'app-user-list-modal',
  templateUrl: './user-list-modal.component.html',
  styleUrls: ['./user-list-modal.component.css']
})
export class UserListModalComponent implements OnInit {
  @Input() user:IUser;


  constructor(public activeModal: BsModalRef) { }

  ngOnInit() {
  }

 
}
