import { userAddress } from './userAddress.model';
import { userCompany } from './userCompany.model';

export interface IUser
{
    id: number;
    name: string;
    username:string;
    email: string;
    address: userAddress;
    phone:string;
    website:string;
    company:userCompany;
}