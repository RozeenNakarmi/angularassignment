import { geoAddress } from './geoAddress.model';

export interface userAddress {
    street:string;
    suite:string;
    city:string;
    zipcode:string;
    geo:geoAddress;
}